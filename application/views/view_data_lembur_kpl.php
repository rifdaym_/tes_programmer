<?php

	echo $this->session->userdata("username");

?>

<a href="<?php echo base_url('login/logout');?>">logout</a>

<html>
	<head></head>

	<body>
		<p><?php echo $title; ?></p>
		<p style="color:red"><?php echo $this->session->flashdata('pesan')?></p>

		<table border="1">
			<thead>
				<tr>
					<th>NIP</th>
					<th>Nama</th>
					<th>Posisi</th>
					<th>Jam Lembur</th>
					<th>Intensif</th>
					<th>Status</th>
					<th>Tanggal Status</th>
					<th>Button</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(empty($data_lembur_kpl)){
				?>
				<tr>
					<td> Data tidak ada</td>
				</tr>
				<?php
				}else{
					foreach ($data_lembur_kpl as $value) {
						$jam=($value->jam_lembur)*20000;
				?>
				<tr>
					<td><?php echo $value->nip?></td>
					<td><?php echo $value->nama?></td>
					<td><?php echo $value->nama_posisi?></td>
					<td><?php echo $value->jam_lembur?></td>
					<td><?php echo "Rp.".$jam?></td>
					<td><?php echo $value->status_lembur?></td>
					<td><?php echo $value->tgl_status?></td>
					<td>
						<a href="<?php echo base_url()?>kepala_divisi/form_edit_status/<?php echo $value->id_klaim?>">Ubah Status</a>
					</td>
				</tr>
				<?php } }?>
			</tbody>
		</table>
	</body>
</html>

