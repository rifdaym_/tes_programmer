<html>
	<head></head>
	<body>
		<p><?php echo $title; ?></p>

		<?php
			foreach($data_status_lembur as $v){
				$id_klaim=$v->id_klaim;
				$nip=$v->nip;
				$nama=$v->nama;
				$nama_posisi=$v->nama_posisi;
				$status_lembur=$v->status_lembur;		
			}
		?>
		<p style="color:red"><?php echo $this->session->flashdata('pesan')?></p>
		<form action="<?php echo base_url('kepala_divisi/action_edit_status') ?>" method="post">
			<table>
				<tr>
					<td>NIP/Nama</td>
					<td><?php echo $nip."/".$nama?></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><?php echo $nama_posisi?></td>
				</tr>
				<tr>
					<td>Status</td>
					<td>
						<select name="status_lembur">
							<option value="<?php echo $status_lembur?>">Pilih Status</option>
							<option value="Approve">Approve</option>
							<option value="Reject">Reject</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<input type="hidden" name="nip" value="<?php echo $nip ?>">
						<input type="hidden" name="id_klaim" value="<?php echo $id_klaim ?>">
						<input type="submit" value="Ubah Status">
						<button type="reset" value="Batal">Batal</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>