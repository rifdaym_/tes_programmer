<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Kepala_divisi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

 		$this->load->model('model_data');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}

	public function index()
	{

		$data['title']='Data Lembur Pegawai';
		$data['data_lembur_kpl'] = $this->model_data->get_data_lembur();

		$this->load->view('view_data_lembur_kpl',$data);

	}

 	function form_edit_status(){

 			$param3 = $this->uri->segment(3);
 	
 		 	$data['title']='Edit Status Klaim Karyawan';
 			$data['data_status_lembur']=$this->model_data->get_data_lemburid($param3);
 			
 			$this->load->view('view_form_edit_status',$data); 	

 	}

 	function action_edit_status(){

 		$nip		= addslashes($this->input->post('nip'));
 		$status_lembur	= addslashes($this->input->post('status_lembur'));
 		$id_klaim	= addslashes($this->input->post('id_klaim'));

			$data = array(
 						'status_lembur' => $status_lembur,
 						'nip'=> $nip,
	 					'tgl_status' => date('Y-m-d')
 					);
 			
 			$this->model_data->get_edit_status($id_klaim,$data);
 			$this->session->set_flashdata("pesan","Status Diperbaharui");
 			redirect('kepala_divisi');

 	}



}

?>
