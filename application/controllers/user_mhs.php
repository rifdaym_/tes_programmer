<?php 

/**
* 
*/
class User_mhs extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}

	function index(){
		$this->load->view('vuser');
	}

	function data_mahasiswa(){
		$data['data_mhs'] = $this->model_admin->get_allmhs();

		$this->load->view('vhome',$data);
	}
}

?>