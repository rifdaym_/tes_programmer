<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
 class Klaim_lembur extends CI_Controller
 {
 	
 	function __construct()
 	{
 		parent::__construct();
 		$this->load->model('model_data');
 		$this->load->helper('form','url');
 	}

 	public function index(){
		$data['title']='Klaim Lembur Pegawai';
		$data['nip'] = $this->model_data->get_nip();

 		$this->load->view('view_form_klaim',$data);
 	}

 	public function add(){

 		$nip		= addslashes($this->input->post('nip'));
 		$jam_lembur	= addslashes($this->input->post('jam_lembur'));
 		$tgl_submit	= addslashes($this->input->post('tgl_submit'));
 		$status_lembur	= addslashes($this->input->post('status_lembur'));


	    $this->form_validation->set_rules('jam_lembur', 'jam_lembur', 'required|trim|numeric');	   
	    if ($this->form_validation->run() == FALSE){
	   		
	   		$data['title']='Klaim Lembur Pegawai';
			$data['nip'] = $this->model_data->get_nip();
		    $this->load->view('view_form_klaim',$data);
	   
	    }else{
		   	    $data1 = array(
	 		  			'nip' => $nip,
	 					'jam_lembur' => $jam_lembur,
	 					'tgl_submit' => date('Y-m-d')
	 				 );


 			$this->model_data->get_insert_klaim($data1,$nip);
 			$this->session->set_flashdata("pesan","Klaim Berhasil Dibuat");
 			redirect('login');
	   }

 	}


 } 
?>